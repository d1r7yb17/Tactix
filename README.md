# Tactix #

![Header Image](tactix.jpg "Tactix")

## Description ##

Tactix is a variant of chess. It is played with dice on a 9x9 board.

Tactix is a new edition by Schmidt Spiele of the Parker game "Duel" from 1975
with slightly different rules (for example Duel uses an 8x9 board).

Each player has nine dice.
Eight of these have the normal dice points from 1 to 6 and
the so-called king's die, only has ones.

The only other material is a game board made up of nine by nine squares.
The sides of the squares are bordered, where the dice can be tilted over,
and that is the permitted method of movement.

In a move, a die can be tilted as many squares as it shows eyes at the beginning.
A one-time, right-angled turn is permitted.

The starting line-up for both players is: 5-1-2-6-K-6-2-1-5.
The king's die in the middle and then next to him, from inside to outside,
a "six", a "two", a "one" and a "five" on top of the dice,
with the "three" pointing to the player and the "four" pointing to the opponent.

It is beaten like in chess.
At the end of the move you reach the field of an opposing "piece",
remove it and take its place with your own "piece".

The aim of the game is either to hit the opposing king's die or
to reach the opposing king's starting square with your own king's die.
Reaching the field is enough to win immediatly.
So it's pointless to cover the own home square against an incoming king.

There is an even shorter version of Tactix,
where it is sufficient to reach the midfield with the king's die.

## Implementation ##

This is an implementation of the shortened version written in Rust.

The main focus will be on a computer-to-computer game engine
in order to compete with other implementations.

However there will be a human vs. computer mode as well for analysis and fun.

## Project Goals ##

- [ ] Basic objects and their interactions (e.g. dice, board and moves)
- [ ] Random move generation
- [ ] First simple computer player
- [ ] Interface to other implementations, including a communication protocol
- [ ] Improved computer player
  - [ ] Negamax
  - [ ] Alpha-Beta Pruning
  - [ ] Iterative Deepening Depth-First Search
  - [ ] Heuristics
  - [ ] Opening Books
- [ ] Friendly UI for human vs. computer games, preferrably a Web-UI
